#pragma once
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "FileContents.h"
#include "editor_state.h"
#include "line_editor.h"
extern struct winsize terminal_size;

#ifndef PRINT_HANDLING
#define PRINT_HANDLING

void _mc(int row, int col);
void clear_screen();
void erase_to_eol();
void draw_editor_text(const FileContents* _fc);
void draw_line_at(int _row);
void draw_line_editor(LineEditor* _l, int _row);
void draw_title_pane(const char* _title);
void draw_command_pane(char command[128]);
void draw_command_field(char command[128]);
void draw_input_pane();
#endif // !PRINT_HANDLING