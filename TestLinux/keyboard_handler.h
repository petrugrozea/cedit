#pragma once
#include <termios.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#ifndef KEYBOARD_HANDLER
#define KEYBOARD_HANDLER
#define KEY_ESCAPE  27
#define KEY_ENTER   13
#define KEY_BACKSPACE 127
#define KEY_UP      65
#define KEY_DOWN    66
#define KEY_LEFT    67
#define KEY_RIGHT   68

void make_raw();
void reset();
int kbhit();
int kbesc();
int kbget();
#endif // !KEYBOARD_HANDLER