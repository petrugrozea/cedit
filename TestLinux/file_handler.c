#include "file_handler.h"

static FileContents fc;

FileContents* get_file_contents()
{
	return &fc;
}

int find_next_eol(char _buffer[256], int _start, int _length)
{
	if (_length > 256)
	{
		fprintf(stderr, "find_next_eol: the _length is bigger than the buffer");
		return -1;
	}

	for (int i = _start; i < _length; i++)
	{
		if (_buffer[i] == '\n')
		{
			return i;
		}
	}

	return -1;
}

void analyze_line(char _buffer[256], FileContents* _fc, Line** _unfinished_line)
{
	int length = strlen(_buffer);
	int bol = 0;
	int eol = -1;
	while ((eol = find_next_eol(_buffer, eol + 1, length)) != -1)
	{
		ptrdiff_t line_length = &_buffer[eol] - &_buffer[bol];
		if (*_unfinished_line != NULL)
		{
			line_concat(*_unfinished_line, &_buffer[bol], line_length);
			fc_add_line(_fc, *(*_unfinished_line));
			*_unfinished_line = NULL;
		}
		else
		{
			fc_add_line(_fc, new_line_from_substring(&_buffer[bol], line_length));
		}

		// update the begining of the line for the next line
		bol = eol + 1;
	}
	if (eol != 254 && length >= 255)
	{
		if (*_unfinished_line == NULL)
		{
			// this means this buffer does not contain the whole line, only the begining of it
			Line l = new_line_from_substring(&_buffer[bol], 255 - bol);
			*_unfinished_line = &l;
		}
		else
		{
			// and this means someone is crazy and has a line bigger than 256 characters, who the fuck does that?
			// and the code below is not tested, some ugly monsters can be summoned if tapping into this code
			line_concat(*_unfinished_line, &_buffer[bol], 255 - bol);
		}
	}
}

char* file_name_from_path(const char* _path)
{
	char* file_name = (char*)malloc(sizeof(char) * 64);
	if (_path[0] == '/')
	{
		int slash_index = -1;
		int length = strlen(_path);
		for (int i = length - 1; i >= 0; i--)
		{
			if (_path[i] == '/')
			{
				slash_index = i;
				break;
			}
		}
		if (slash_index != -1)
		{
			strncpy(file_name, &_path[slash_index + 1], length - slash_index);
			return file_name;
		}
		else
		{
			strcpy(file_name, _path);
			return file_name;
		}
	}
	else 
	{
		strcpy(file_name, _path);
		return file_name;
	}
}

char* path_to_absolute(const char* _path)
{
	if (_path[0] == '/')
	{
		return _path;
	}
	else
	{
		char* buf = (char*)malloc(sizeof(char) * 512);
		if (getcwd(buf, 512) != NULL)
		{
			strcat(buf, "/");
			strcat(buf, _path);
			return buf;
		}
		else {
			return _path;
		}
	}
}

char* path_to_dir(const char* _path)
{
	char* buf = (char*)malloc(sizeof(char) * 512);
	if (_path[0] == '/')
	{
		int slash_index = -1;
		int length = strlen(_path);
		for (int i = length - 1; i >= 0; i--)
		{
			if (_path[i] == '/')
			{
				slash_index = i;
				break;
			}
		}
		if (slash_index != -1)
		{
			strncpy(buf, _path, slash_index + 1);
		}
		else
		{
			return _path;
		}
	}
	else
	{
		if (getcwd(buf, 512) == NULL)
		{
			return _path;
		}
	}

	return buf;
}

void load_file(const char* _file_path)
{
	if (strlen(_file_path) == 0)
	{
		fprintf(stderr, "File path empty or null\n");
		exit(1);
	}
	fc = new_file_contents(file_name_from_path(_file_path), path_to_dir(_file_path));

	int file_descriptor = open(path_to_absolute(_file_path), O_RDONLY);
	if (file_descriptor == -1)
	{
		// this means that we'll work with a new file
		return;
	}

	char buffer[256];
	buffer[255] = '\0';
	Line* unfinished_line = NULL;
	while (read(file_descriptor, buffer, 255) != 0)
	{
		analyze_line(buffer, &fc, &unfinished_line);
	}
	close(file_descriptor);
}

int save_file()
{
	return save_file_to(fc.filename);
}

int save_file_to(const char* _file_path)
{
	char path[512];
	if (strlen(_file_path) == 0)
	{
		return save_file();
	}
	if (_file_path[0] == '/')
	{
		strcpy(path, _file_path);
	}
	else
	{
		strcpy(path, fc.path);
		strcat(path, _file_path);
	}

	int fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IROTH | S_IWOTH);
	if (fd == -1)
	{
		return 0;
	}

	for (int i = 0; i < fc.size; i++)
	{
		write(fd, fc.lines[i].content, fc.lines[i].length);
		write(fd, "\n", 1);
	}

	close(fd);
	return 1;
}