#pragma once
#include <stdlib.h>
#include <string.h>
#include "Line.h"

#ifndef LINE_EDITOR
#define LINE_EDITOR

typedef struct LineEditor
{
	char before[512];
	char after[512];
	int b_size;
	int a_size;
	int modified;
}LineEditor;

LineEditor new_line_editor(const char* _line, int _split_index);
int line_editor_move_left(LineEditor* _l);
int line_editor_move_right(LineEditor* _l);
int line_editor_add(LineEditor* _l, char c);
int line_editor_remove(LineEditor* _l);
Line line_editor_to_line(LineEditor* _l);
Line line_editor_eol(LineEditor* _l);
int line_editor_length(LineEditor* _l);

#endif // !LINE_EDITOR