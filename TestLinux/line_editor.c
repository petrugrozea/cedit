#include "line_editor.h"

LineEditor new_line_editor(const char* _line, int _split_index)
{
	LineEditor le = { .b_size = 0,.a_size = 0,.modified = 0 };
	memset(le.before, '\0', 512);
	memset(le.after, '\0', 512);

	if (strlen(_line) != 0)
	{
		strncpy(le.before, _line, _split_index);
		le.b_size = _split_index;

		le.a_size = strlen(_line) - _split_index;
		strncpy(&le.after[511 - le.a_size], &_line[_split_index], le.a_size);
	}

	return le;
}

int line_editor_move_left(LineEditor* _l)
{
	if (_l->a_size < 511)
	{
		if (_l->b_size == 0)
		{
			return 0;
		}

		_l->a_size++;
		_l->b_size--;
		_l->after[511 - _l->a_size] = _l->before[_l->b_size];
		_l->before[_l->b_size] = '\0';
		return 1;
	}

	return -1;
}

int line_editor_move_right(LineEditor* _l)
{
	if (_l->b_size < 511)
	{
		if (_l->a_size == 0)
		{
			return 0;
		}

		_l->before[_l->b_size] = _l->after[511 - _l->a_size];
		_l->b_size++;
		_l->after[511 - _l->a_size] = '\0';
		_l->a_size--;
		return 1;
	}

	return -1;
}

int line_editor_add(LineEditor* _l, char c)
{
	if (_l->b_size < 511)
	{
		_l->before[_l->b_size++] = c;
		_l->before[_l->b_size] = '\0';
		_l->modified = 1;
		return 1;
	}

	return 0;
}

int line_editor_remove(LineEditor* _l) {
	if (_l->b_size > 0)
	{
		_l->before[--_l->b_size] = '\0';
		_l->modified = 1;
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}

Line line_editor_to_line(LineEditor* _l)
{
	Line l = new_line(_l->before);
	line_concat(&l, &_l->after[511 - _l->a_size], _l->a_size);
	return l;
}

Line line_editor_eol(LineEditor* _l)
{
	Line l = {};
	if (_l->a_size == 0)
	{
		l = empty_line();
	}
	else
	{
		l = new_line(&_l->after[511 - _l->a_size]);
	}
	memset(_l->after, '\0', 512);
	_l->a_size = 0;
	_l->modified = 1;
	return l;
}

int line_editor_length(LineEditor* _l)
{
	return _l->b_size + _l->a_size;
}