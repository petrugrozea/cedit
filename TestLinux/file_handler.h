#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "FileContents.h"
#include "Line.h"

#ifndef FILE_HANDLER
#define FILE_HANDLER

FileContents* get_file_contents();
int find_next_eol(char _buffer[256], int _start, int _length);
void analyze_line(char _buffer[256], FileContents* _fc, Line** _unfinished_line);
char* file_name_from_path(const char* _path);
char* path_to_absolute(const char* _path);
void load_file(const char* _file_path);
int save_file();
int save_file_to(const char* _file_path);

#endif // !FILE_HANDLER
