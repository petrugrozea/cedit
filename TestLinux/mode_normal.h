#pragma once
#include "FileContents.h"
#include "print_handling.h"
#include "keyboard_handler.h"
#include "file_handler.h"
#include "s_words.h"
extern void shutdown();
extern void set_mode_input();

#ifndef MODE_NORMAL
#define MODE_NORMAL
void set_mode_normal();
void command_input(char c);
void command_backspace();
void command_clear();
void set_response(char* _message);
void analyze_command();
char* get_command();
void exit_normal_mode();
#endif // !MODE_NORMAL