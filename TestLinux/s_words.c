#include "s_words.h"

string_list new_string_list(int capacity)
{
	string_list list = { .count = 0,.capacity = capacity };
	list.strings = (char**)malloc(sizeof(char*) * list.capacity);
	return list;
}

int string_list_add(string_list* _slist, char* word)
{
	if (_slist->count < _slist->capacity)
	{
		_slist->strings[_slist->count] = word;
		_slist->count++;
		return 1;
	}

	return 0;
}

char* substring(const char* _string, int _begin, int _end)
{
	int length = _end - _begin + 1;
	char* buf = (char*)malloc(sizeof(char) * length + 1);
	strncpy(buf, &_string[_begin], length);
	buf[length] = '\0';
	return buf;
}

string_list string_to_word_list(const char* _string, int _max_words)
{
	string_list l = new_string_list(_max_words);
	int length = strlen(_string);
	for (int i = 0, b_index = 0; i < length; i++)
	{
		if (_string[i] == ' ')
		{
			if (!string_list_add(&l, substring(_string, b_index, i - 1)))
			{
				break;
			}
			b_index = i + 1;
		}

		if (i == length - 1)
		{
			if (b_index != i && _string[i] != ' ')
			{
				string_list_add(&l, substring(_string, b_index, i));
			}
		}
	}

	return l;
}

// why I did this and not used strcmp? Because fuck bytes and symbols and all the misteries this world has :|
int compare_cmd(const char* _f, const char* _cmd) {
	int length = strlen(_cmd);
	if (strlen(_f) < length)
	{
		return 0;
	}

	for (int i = 0; i < length; i++)
	{
		if (_f[i] != _cmd[i])
		{
			return 0;
		}
	}

	return 1;
}

void string_list_free(string_list* _l)
{
	for (int i = 0; i < _l->count; i++) {
		free(_l->strings[i]);
	}
	free(_l->strings);
}