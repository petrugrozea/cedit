#include "keyboard_handler.h"

static struct termios term, oterm;

void make_raw()
{
	tcgetattr(STDIN_FILENO, &oterm);
	memcpy(&term, &oterm, sizeof(term));
	term.c_lflag &= ~(ICANON | ECHO | ISIG);
	term.c_iflag &= ~(ICRNL);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

void reset()
{
	tcsetattr(STDIN_FILENO, TCSANOW, &oterm);
}

static int _getch()
{
	return getchar();
}

int kbhit()
{
	term.c_cc[VMIN] = 0;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);

	int c = _getch();

	term.c_cc[VMIN] = 1;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
	
	if (c != -1) 
		ungetc(c, stdin);
	return (c != -1) ? 1 : 0;
}

int kbesc()
{
	int c = _getch();

	if (!kbhit()) 
		return KEY_ESCAPE;

	if (c == '[')
	{
		switch (_getch())
		{
			case 'A':
				c = KEY_UP;
				break;
			case 'B':
				c = KEY_DOWN;
				break;
			case 'C':
				c = KEY_RIGHT;
				break;
			case 'D':
				c = KEY_LEFT;
				break;
			default:
				c = 0;
				break;
		}
	}
	else
	{
		c = 0;
	}
	if (c == 0) while (kbhit()) _getch();
	return c;
}

int kbget()
{
	int c = _getch();
	return c == KEY_ESCAPE ? kbesc() : c;
}