#include "mode_input.h"

static int _run = 1;
static LineEditor editor = { .modified = 0,.a_size = 0,.b_size = 0 };

void set_mode_input()
{
	FileContents* _fc = get_file_contents();
	draw_title_pane(_fc->filename);
	draw_editor_text(_fc);
	draw_input_pane();

	make_raw();
	put_cursor_inside_panel();
	load_line();
	int key;
	while (_run)
	{
		key = kbget();
		switch (key)
		{
		case KEY_UP:
			save_line();
			if(scroll_up())
				load_line();
			break;
		case KEY_DOWN:
			save_line();
			if(scroll_down())
				load_line();
			break;
		case KEY_LEFT:
			if (scroll_left())
			{
				line_editor_move_left(&editor);
				if(get_cursor_col() == 1)
					draw_line_editor(&editor, get_cursor_row());
			}
			break;
		case KEY_RIGHT:
			if (scroll_right_l(line_editor_length(&editor)))
			{
				line_editor_move_right(&editor);
				if (get_cursor_col() == terminal_size.ws_col)
					draw_line_editor(&editor, get_cursor_row());
			}
			break;
		case KEY_BACKSPACE:
			switch (line_editor_remove(&editor))
			{
			case 1:
				scroll_left();
				draw_line_editor(&editor, get_cursor_row());
				break;
			case -1:
				save_line();
				scroll_up();
				go_to_eol();
				fc_remove_line(_fc, get_cursor_actual_line() + 1);
				load_line();
				draw_editor_text(_fc);
				break;
			}
			break;
		case KEY_ENTER:
			enter_new_line();
			break;
		case KEY_ESCAPE:
			exit_input_mode();
			break;
		default:
			line_editor_add(&editor, key);
			scroll_right_l(line_editor_length(&editor));
			draw_line_editor(&editor, get_cursor_row());
			break;
		}
	}
	save_line();
	// for next time to work :D
	_run = 1;
}

void save_line()
{
	FileContents* fc = get_file_contents();
	if (editor.modified == 1)
	{
		free_line(&fc->lines[get_cursor_actual_line()]);
		fc->lines[get_cursor_actual_line()] = line_editor_to_line(&editor);
		editor.modified = 0;
	}
	
}

void load_line()
{
	FileContents* fc = get_file_contents();
	editor = new_line_editor(fc->lines[get_cursor_actual_line()].content, get_cursor_actual_col());
}

void enter_new_line()
{
	FileContents* fc = get_file_contents();
	fc_insert_line(fc, line_editor_eol(&editor), get_cursor_actual_line() + 1);
	move_cursor(get_cursor_row(), 1);
	for (int i = get_cursor_actual_col(); i > 0; i--)
	{
		line_editor_move_left(&editor);
	}
	save_line();
	scroll_down();
	load_line();
	draw_editor_text(fc);
}

void exit_input_mode()
{
	reset();
	_run = 0;
}