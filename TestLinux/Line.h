#pragma once
#include <stdlib.h>
#include <string.h>

#ifndef LINE
#define LINE

typedef struct Line
{
	int length;
	char* content;
}Line;

Line empty_line();
Line new_line_from_substring(char*, const int);
Line new_line(char*);
void line_concat(Line*, char*, const int);
void free_line(Line* _l);
#endif