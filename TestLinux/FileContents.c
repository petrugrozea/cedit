#include "FileContents.h"

FileContents new_file_contents(char* _filename, char* _path)
{
	FileContents fc = { .size = 0, .capacity = 10, .filename = _filename, .path = _path };
	fc.lines = (Line*)malloc(sizeof(Line) * fc.capacity);
	return fc;
}

void fc_extend_capacity(FileContents* _fc)
{
	_fc->capacity += 10;
	void* tmp = realloc(_fc->lines, sizeof(Line) * _fc->capacity);
	if (tmp == NULL)
	{
		// I guess there is nothing I can do here. No error handling :(
		// Bad things will happen after this :)
		return;
	}
	_fc->lines = tmp;
}

void fc_add_line(FileContents* _fc, Line _l)
{
	if (_fc->size == _fc->capacity)
	{
		fc_extend_capacity(_fc);
	}
	_fc->lines[_fc->size++] = _l;
}

void fc_insert_line(FileContents* _fc, Line _l, int _at)
{
	if (_at < 0 || _at >= _fc->size)
	{
		// if insert at the end of the file
		if (_at == _fc->size)
		{
			fc_add_line(_fc, _l);
		}
		return;
	}
	if (_fc->size == _fc->capacity)
	{
		fc_extend_capacity(_fc);
	}

	memmove(&_fc->lines[_at + 1], &_fc->lines[_at], sizeof(Line) * (_fc->size - _at));
	_fc->lines[_at] = _l;
	_fc->size++;
}

void fc_remove_line(FileContents* _fc, int _at)
{
	if (_at <= 0 || _at >= _fc->size)
		return;

	line_concat(&_fc->lines[_at - 1], _fc->lines[_at].content, _fc->lines[_at].length);

	free_line(&_fc->lines[_at]);
	if (_at != _fc->size - 1)
	{
		memmove(&_fc->lines[_at], &_fc->lines[_at + 1], sizeof(Line) * (_fc->size - _at - 1));
	}
	_fc->size--;
}