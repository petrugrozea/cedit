#include "main.h"

void launch(char* _path)
{
	load_file(_path);
	update_terminal_data();
	while(1)
		set_mode_normal();
}

void shutdown()
{
	// and all the clear-up is done here
	reset();
	clear_screen();
	exit(0);
	// yeah, that was it, thanks for using our app :)
}

int main(int argc, const char* argv[])
{
	if(argc > 1)
		launch(argv[1]);

	fprintf(stderr, "File name needed after ./cedit {file name}");
	return -1;
}