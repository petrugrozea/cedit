#include "mode_normal.h"

static char command[128];
static int _run = 1;

void set_mode_normal()
{
	FileContents* _fc = get_file_contents();
	draw_title_pane(_fc->filename);
	draw_editor_text(_fc);
	memset(command, '\0', 128);
	draw_command_pane(command);

	make_raw();
	put_cursor_inside_panel();
	int key;
	while (_run)
	{
		key = kbget();
		switch (key)
		{
		case KEY_UP:
			scroll_up();
			break;
		case KEY_DOWN:
			scroll_down();
			break;
		case KEY_LEFT:
			scroll_left();
			break;
		case KEY_RIGHT:
			scroll_right();
			break;
		case KEY_BACKSPACE:
			command_backspace();
			break;
		case KEY_ENTER:
			analyze_command();
			break;
		case KEY_ESCAPE:
			break;
		default:
			command_input(key);
			break;
		}
	}

	_run = 1;
}

void command_input(char c)
{
	int length = strlen(command);
	if (length < 126)
	{
		command[length] = c;
		command[length + 1] = '\0';
		draw_command_field(command);
	}
}

void command_backspace()
{
	int length = strlen(command);
	if (length > 0)
	{
		command[length - 1] = '\0';
		draw_command_field(command);
	}
}

void command_clear()
{
	command[0] = '\0';
}

void set_response(char* _message)
{
	draw_command_field(_message);
}

void set_prompt(char* _message, void(*fun_ptr)())
{
	draw_command_field(_message);
	while (1)
	{
		char c = getchar();
		if (c == 'y' || c == 'Y')
		{
			fun_ptr();
			return;
		}
		else
		{
			if (c == 'n' || c == 'N')
			{
				command_clear();
				draw_command_field(command);
				return;
			}
		}
	}
}

void analyze_command()
{
	string_list l = string_to_word_list(command, 4);
	command_clear();
	if (l.count > 0)
	{
		char* cmd = l.strings[0];
		if (compare_cmd(cmd, ":i"))
		{
			exit_normal_mode();
			set_mode_input();
			_run = 0;
		}
		else if (compare_cmd(cmd, ":q"))
		{
			set_prompt("Quitting will discard all your progress. Are you sure?(Y/n)", shutdown);

		}
		else if (compare_cmd(cmd, ":w"))
		{
			int status = 0;
			if (l.count >= 2)
			{
				status = save_file_to(l.strings[1]);
			}
			else
			{
				status = save_file();
			}

			if (status)
			{
				set_response("File saved!");
			}
			else
			{
				set_response("Abort. An error occured while saving the file.");
			}
		}
		else
		{
			set_response("Invalid command!");
		}
	}
	else
	{
		draw_command_field(command);
	}
	string_list_free(&l);
}

char* get_command()
{
	return command;
}

void exit_normal_mode()
{
	clear_screen();
	reset();
}