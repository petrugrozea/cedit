#include "print_handling.h"

// this is for optimization purposes
static char _mc_buf[20];
void _mc(int row, int col)
{
	sprintf(_mc_buf, "\033[%d;%dH", row, col);
	write(STDOUT_FILENO, _mc_buf, strlen(_mc_buf));
}

void clear_screen()
{
	write(STDOUT_FILENO, "\033c\e[3J", 7);
}

void erase_to_eol()
{
	write(STDOUT_FILENO, "\033[K", 4);
}

void draw_line_at(int _row)
{
	_mc(_row, 0);
	for (int i = 0; i < terminal_size.ws_col; i++)
	{
		write(STDOUT_FILENO, "-", 1);
	}
	cursor_rebalance();
}

void draw_line_editor(LineEditor* _l, int _row)
{
	_mc(_row, 1);
	unsigned editor_size = terminal_size.ws_col;
	int skipped = get_first_col();
	int b_av = strlen(&_l->before[skipped]);
	int a_av = _l->a_size > editor_size - b_av ? editor_size - b_av : _l->a_size;

	write(STDOUT_FILENO, &_l->before[skipped], strlen(&_l->before[skipped]));
	write(STDOUT_FILENO, &_l->after[511 - _l->a_size], a_av);
	erase_to_eol();
	cursor_rebalance();
}

void draw_editor_text(const FileContents* _fc) 
{
	unsigned editor_size = terminal_size.ws_row - 4;
	_mc(3, 1);
	int available_lines = _fc->size > editor_size ? editor_size : _fc->size;
	for (int i = 0; i < available_lines; i++)
	{
		Line* l = &_fc->lines[get_first_line() + i];
		_mc(3 + i, 0);
		int to_write_count = strlen(l->content) < terminal_size.ws_col ? strlen(l->content) - get_first_col() : terminal_size.ws_col;
		to_write_count = to_write_count < 0 ? 0 : to_write_count;
		write(STDOUT_FILENO, &l->content[get_first_col()], to_write_count);
		erase_to_eol();
	}
	cursor_rebalance();
}

void draw_title_pane(const char* _title)
{
	unsigned center = (terminal_size.ws_col - strlen(_title)) / 2;
	_mc(1, center);
	write(STDOUT_FILENO, _title, strlen(_title));
	draw_line_at(2);
	cursor_rebalance();
}

void draw_command_pane(char command[128])
{
	draw_line_at(terminal_size.ws_row - 1);
	draw_command_field(command);
}

void draw_command_field(char command[128])
{
	_mc(terminal_size.ws_row, 0);
	erase_to_eol();
	write(STDOUT_FILENO, command, strlen(command));
	cursor_rebalance();
}

void draw_input_pane()
{
	draw_line_at(terminal_size.ws_row - 1);
	_mc(terminal_size.ws_row, 0);
	erase_to_eol();
	write(STDOUT_FILENO, "-- INPUT MODE --", 17);
	cursor_rebalance();
}