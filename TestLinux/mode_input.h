#pragma once
#include "file_handler.h"
#include "print_handling.h"
#include "editor_state.h"
#include "keyboard_handler.h"
#include "line_editor.h"
extern void set_mode_normal();

#ifndef MODE_INPUT
#define MODE_INPUT

void set_mode_input();
void exit_input_mode();

#endif // !MODE_INPUT
