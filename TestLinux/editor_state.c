#include "editor_state.h"

static int first_line = 0;
static int first_col = 0;
static int cursor_row = 0;
static int cursor_col = 1;

struct winsize terminal_size = { 0,0,0,0 };

void update_terminal_data()
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	if (terminal_size.ws_col != w.ws_col || terminal_size.ws_row != w.ws_row)
	{
		terminal_size = w;
		on_resize();
	}
}

void on_resize()
{
	clear_screen();
	draw_title_pane(get_file_contents()->filename);
	draw_editor_text(get_file_contents());
	draw_command_pane(get_command());
	cursor_rebalance();
}

void move_cursor(int row, int col)
{
	set_cursor_position(row, col);
	_mc(cursor_row, cursor_col);
}

void cursor_rebalance()
{
	_mc(cursor_row, cursor_col);
}

void put_cursor_inside_panel()
{
	if (get_cursor_row() < 3 || get_cursor_row() > terminal_size.ws_row - 2)
	{
		move_cursor(3, 0);
	}
}

void set_cursor_position(int row, int col)
{
	cursor_row = row >= 0 && row <= terminal_size.ws_row ? row : 0;
	cursor_col = col >= 1 && col <= terminal_size.ws_col ? col : 1;
}

int get_cursor_row()
{
	return cursor_row;
}

int get_cursor_col()
{
	return cursor_col;
}

int get_first_col()
{
	return first_col;
}

int get_first_line()
{
	return first_line;
}

int get_cursor_actual_col()
{
	return first_col + cursor_col - 1;
}

int get_cursor_actual_line()
{
	return first_line + cursor_row - 3;
}

int _rebalance_column()
{
	int line_length = get_file_contents()->lines[cursor_row - 3 + first_line].length;
	if (first_col + cursor_col > line_length)
	{
		if (line_length - first_col > 0) 
		{
			cursor_col = line_length - first_col + 1;
			return 0;
		}
		else if (line_length == 0)
		{
			first_col = 0;
			cursor_col = 1;
			return 1;
		}
		else
		{
			first_col = line_length - 1;
			cursor_col = 1;
			return 1;
		}
	}
}

int scroll_up()
{
	if (cursor_row == 3)
	{
		if (first_line <= 0)
		{
			first_line = 0;
			return 0;
		}
		first_line--;
		draw_editor_text(get_file_contents());
		return 1;
	}
	else
	{
		--cursor_row;
		if (_rebalance_column())
			draw_editor_text(get_file_contents());
		move_cursor(cursor_row, cursor_col);
		return 1;
	}
}

int scroll_down()
{
	FileContents* fc = get_file_contents();
	if (cursor_row == terminal_size.ws_row - 2)
	{
		if (first_line >= fc->size - terminal_size.ws_row)
		{
			first_line = fc->size - terminal_size.ws_row;
			return 0;
		}
		first_line++;
		draw_editor_text(fc);
		return 1;
	}
	else 
	{
		++cursor_row;
		if (_rebalance_column())
			draw_editor_text(fc);
		move_cursor(cursor_row, cursor_col);
		return 1;
	}
}

int scroll_left()
{
	FileContents* fc = get_file_contents();
	if (cursor_col > 1)
	{
		move_cursor(cursor_row, --cursor_col);
		return 1;
	}
	else
	{
		if (first_col != 0) {
			first_col--;
			draw_editor_text(fc);
			return 1;
		}
	}
	return 0;
}

void go_to_eol()
{
	while (scroll_right() != 0);
}

int scroll_right()
{
	return scroll_right_l(get_file_contents()->lines[cursor_row - 3 + first_line].length);
}

int scroll_right_l(int _length)
{
	if (first_col + cursor_col <= _length)
	{
		if (cursor_col < terminal_size.ws_col)
		{
			move_cursor(cursor_row, ++cursor_col);
			return 1;
		}
		else
		{
			first_col++;
			draw_editor_text(get_file_contents());
			return 1;
		}
	}
	return 0;
}