#pragma once
#include "FileContents.h"
#include "print_handling.h"
#include "file_handler.h"
#include "mode_normal.h"
#ifndef EDITOR_STATE
#define EDITOR_STATE

void update_terminal_data();
void on_resize();
void move_cursor(int row, int col);
void cursor_rebalance();
void put_cursor_inside_panel();
void set_cursor_position(int row, int col);
int get_cursor_row();
int get_cursor_col();
int get_first_col();
int get_first_line();
int get_cursor_actual_col();
int get_cursor_actual_line();
int scroll_up();
int scroll_down();
int scroll_left();
void go_to_eol();
int scroll_right();
int scroll_right_l(int _length);

#endif // !EDITOR_STATE