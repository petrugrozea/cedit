#include "Line.h"

Line empty_line()
{
	Line l = { .length = 0 };
	l.content = (char*)malloc(sizeof(char));
	l.content[0] = '\0';
	return l;
}

Line new_line_from_substring(char* _substring, const int _length)
{
	Line l = { .length = _length};
	// the + 1 is for the \0 character, not sure if needed in this case, but let it be
	// tho there are still some "too many" characters sometimes
	l.content = (char*)malloc(sizeof(char) * l.length + 1);
	strncpy(l.content, _substring, l.length);
	l.content[l.length] = '\0';
	return l;
}

Line new_line(char* _content)
{
	Line l = { .length = strlen(_content)};
	l.content = (char*)malloc(sizeof(char) * l.length + 1);
	strncpy(l.content, _content, l.length);
	l.content[l.length] = '\0';
	return l;
}

// this thing may cause errors, this pointers drive me crazy :)
void line_concat(Line* _line, char* _string, const int _str_length)
{
	if (_str_length <= 0)
	{
		return;
	}

	_line->length = _line->length + _str_length;
	void* tmp = realloc(_line->content, _line->length + 1);
	if (tmp == NULL)
	{
		return;
	}
	_line->content = tmp;
	strncat(_line->content, _string, _str_length);
	_line->content[_line->length] = '\0';
}

void free_line(Line* _l)
{
	_l->length = 0;
	free(_l->content);
}