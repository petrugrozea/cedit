#pragma once
#include <stdlib.h>
#include <string.h>

#ifndef S_WORDS
#define S_WORDS

typedef struct string_list
{
	char** strings;
	int count;
	int capacity;
}string_list;

string_list new_string_list(int capacity);
int string_list_add(string_list* _slist, char* word);
char* substring(const char* _string, int _begin, int _end);
string_list string_to_word_list(const char* _string, int _max_words);
void string_list_free(string_list* _l);
int compare_cmd(const char* _f, const char* _cmd);

#endif // !S_WORDS