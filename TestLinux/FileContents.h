#pragma once
#include <stdlib.h>
#include "Line.h"

#ifndef FILE_CONTENTS
#define FILE_CONTENTS

typedef struct FileContents
{
	char* filename;
	char* path;
	int size;
	int capacity;
	Line* lines;
}FileContents;

FileContents new_file_contents(char* _filename, char* _path);
void fc_extend_capacity(FileContents*);
void fc_add_line(FileContents*, Line);
void fc_insert_line(FileContents* _fc, Line _l, int _at);
void fc_remove_line(FileContents* _fc, int _at);
#endif