The project has completed only the Phase I from the requirements

Compilation:
It can be compiled by running the "make" command in the same folder as the makefile "/TestLinux"

Launch:
Launch from the terminal the program found int "../TestLinux/TestLinux/cedit" with {file_path} as parameter
example of launch command:
./cedit myTextFile.txt

Features:
It contains most of the features I could think of of an simple text editor.

Not finished/Bugs/Limitations:
- ...maybe more of them, I haven't really went through testing the app because of time limit